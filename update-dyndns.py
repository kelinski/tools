#!/usr/bin/env python3

from optparse import OptionParser
import json
import MySQLdb
import time
import re
import sys

def is_valid_ip(ip):
    m = re.match(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$", ip)
    return bool(m) and all(map(lambda n: 0 <= int(n) <= 255, m.groups()))

parser = OptionParser()
parser.add_option("-u", "--user", dest="user",
					help="username for authentication", metavar="USER")

parser.add_option("-p", "--password", dest="password",
					help="password for authentication", metavar="PASSWORD")

parser.add_option("-a", "--address", dest="ip",
                    help="new IP-address for updating", metavar="address")

(options, args) = parser.parse_args()

reString = re.compile("^[a-zA-Z0-9]{1,64}$")

if not reString.match(options.user):
    print ("The username should only contain ascii characters and numbers and should be not longer than 64 chars.")
    print ("FAILED")
    sys.exit()

if not reString.match(options.password):
    print ("The password should only contain ascii characters and numbers and should be not longer than 64 chars.")
    print ("FAILED")
    sys.exit()

if not is_valid_ip(options.ip):
    print ("The given address is not valid")
    print ("FAILED")
    sys.exit()

with open('/etc/credentials/dyndns/pdns-mysql.json') as json_file:
    config = json.load(json_file)

print (config)

# Connect for AUTH.
authConnection = MySQLdb.connect(
    host    = config['authHost'],
    user    = config['authUser'],
    passwd  = config['authPassword'],
    db      = config['authDB'])

authCursor  = authConnection.cursor()

# Connect for Update.
# This connects to the PowerDNS MySQL Backend
pdnsConnection = MySQLdb.connect(
    host    = config['pdnsHost'],
    user    = config['pdnsUser'],
    passwd  = config['pdnsPassword'],
    db      = config['pdnsDB'])
pdnsCursor  = pdnsConnection.cursor()

# Holts a unique set of Zones, so that no zone is updated twice
zones = set()

# Select from the AUTH DB all Hostnames and the responding zone to update
authCursor.execute( "SELECT id, hostname, zone FROM auth WHERE user = '{0}' AND password = '{1}'".format(options.user, options.password) )

# Loops through the Results
if authCursor.rowcount > 0:
    for host in authCursor.fetchall():
#        print "UPDATE records SET content = '{0}' WHERE name = '{1}' AND type = 'A'".format( options.ip, host[1] )

        # Get the current content of the A record
        pdnsCursor.execute("SELECT id, name, content FROM records WHERE name = '{0}' AND type = 'A'".format(
            host[1]
        ))

        # Check that there is exact 1 row, otherwise there's something wrong
        if pdnsCursor.rowcount == 1:
            # Okay, if current and new IP differes, it has to be set
            currentRecord = pdnsCursor.fetchone()
            if currentRecord[2] != options.ip:
                # Okay, it differs, so it has to be updated
                zones.add(host[2])
                # Update the A Record
                pdnsCursor.execute("UPDATE records SET content = '{0}' WHERE name = '{1}' AND type = 'A'".format(
                    options.ip, host[1]
                ))
                pdnsConnection.commit()
#               print pdnsCursor.fetchall()
            else:
                print ("{0}: IP Address is the same, but this is okay".format(host[1]))
            #
        else:
            print ("More or less then one A Record for this, thats not okay")
        #
    #
else:
    print ("FAILED")
    sys.exit()
#

if len(zones) > 0:
    for zone in zones:
        pdnsCursor.execute("SELECT id, name, content FROM records WHERE name = '%s' AND type = 'SOA'" % zone)
        for SOA in pdnsCursor.fetchall():
            #print "SOA: %s %s" % (SOA[1], SOA[2])
            SOADetails=SOA[2].split()
#            print "Master  = %s" % SOADetails[0]
#            print "Mail    = %s" % SOADetails[1]
#            print "Serial  = %s" % SOADetails[2]
#            print "Refresh = %s" % SOADetails[3]
#            print "Retry   = %s" % SOADetails[4]
#            print "Expire  = %s" % SOADetails[5]
#            print "TTL     = %s" % SOADetails[6]

            SOADetails[2] = str(int(time.time()))
#            print " ".join(SOADetails)

            pdnsCursor.execute("UPDATE records SET content = '{0}' WHERE id = '{1}' AND name = '{2}' AND type = 'SOA'".format(
                    " ".join(SOADetails), SOA[0], SOA[1]) )
            pdnsConnection.commit()
#            print pdnsCursor.fetchall()
        #
    #
else:
    print ("No zones to update, but this is okay")
#

print ("SUCCESS")

# Close the connections
authConnection.close()
pdnsConnection.close()
