@echo off
::SETLOCAL

:: Aktuelles Verzeichnis ermitteln
SET LOCATION=%~dp0

:: Zu sichernde Unterverzeichnisse setzten
:: "Documents" "Downloads" "Music" "Pictures" "Saved Games" "Videos"
SET SUBDIRS="Desktop" "Pictures" "Documents"

:: Paramter ermitteln / setzten. Bei Bedarf anpassen
SET QUELLE=C:\Users\Chris\
SET ZIEL=%LOCATION%Christian\
SET LOG=%LOCATION%backup.log
.. For dry run add /L to params
SET PARAMS=/MIR /COPY:DAT /DCOPY:T /LOG+:"%LOG%" /TEE /R:3 /W:3

echo Quelle   : "%QUELLE%"
echo Ziel     : "%ZIEL%"
echo Log      : "%LOG%"
echo Parameter: %PARAMS%

echo robocopy "%QUELLE%" "%ZIEL%" %PARAMS%

:: Nacheinander alle Verzeichnisse sichern
for %%d in (%SUBDIRS%) do (
   echo Sicherung von "%QUELLE%%%~d"
   robocopy "%QUELLE%%%~d" "%ZIEL%%%~d" %PARAMS%
)
